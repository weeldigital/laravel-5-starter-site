<?php namespace Starter\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest {

    /**
     * Attributes that are fillable are also
     * the ones that get rules set.
     * @param $rules
     * @param $fillable
     */
	public function fillableRules($rules, $fillable)
    {
        foreach($rules as $rule => $definition){
            if(!array_key_exists($rule, $fillable))
            {
                unset($rules[$rule]);
            }
        }
        return $rules;
    }

}
