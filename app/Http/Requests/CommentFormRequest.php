<?php
namespace Starter\Http\Requests;

use Starter\Models\Comment;

class CommentFormRequest extends Request
{
    /**
     * @return mixed
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the rules for the validation of the form.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content' => 'required'
        ];
    }
}